package cmsc433.p3;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class StudentMTMazeSolver_BFS extends SkippingMazeSolver {

	static Choice ch;

	static boolean found = false;

	List<Direction> pathToFullPath = new ArrayList<Direction>();

	ConcurrentLinkedQueue<SolutionNode> frontier = new ConcurrentLinkedQueue<SolutionNode>();

	Object waitLock = new Object();

	Direction exploring = null;

	public List<SolutionNode> expand(SolutionNode node) throws SolutionFound {
		LinkedList<SolutionNode> result = new LinkedList<SolutionNode>();
		if (maze.display != null)
			maze.setColor(node.choice.at, 0);
		for (Direction dir : node.choice.choices) {
			exploring = dir;
			Choice newChoice = follow(node.choice.at, dir);
			if (maze.display != null)
				maze.setColor(newChoice.at, 2);
			result.add(new SolutionNode(node, newChoice));
		}
		return result;
	}

	public StudentMTMazeSolver_BFS(Maze maze) {
		super(maze);
	}

	public List<Direction> solve() {
		SolutionNode curr = null;

		int threadCount = 100;
		MazeThread[] allThreads = new MazeThread[threadCount];

		// choiceStack.push(firstChoice(maze.getStart()));
		// addToChoiceStack(firstChoice(maze.getStart()));
		try {
			frontier.offer(new SolutionNode(null, firstChoice(maze.getStart())));
			for (int i = 0; i < threadCount; i++) {
				allThreads[i] = new MazeThread();

				allThreads[i].start();

			}

			for (int i = 0; i < threadCount; i++) {
				try {
					allThreads[i].join();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		} catch (SolutionFound e) {

		}

		return pathToFullPath;

	}

	class MazeThread extends Thread {

		public void run() {

			SolutionNode curr = null;

			if (found) {
				//System.out.println("yeilding thread");
				Thread.yield();
				return;
			}

			while (!found) {

				synchronized (waitLock) {
					// System.out.println("Thread started" + this + "found: " +
					// found);
					while (frontier.isEmpty()) {
						try {
							//// System.out.println("waiting");
							waitLock.wait();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}

					if (found) {
						// System.out.println("yeilding thread");
						Thread.yield();
						break;
						// return;
					}
					SolutionNode node = null;
					try {
						if (found)
							break;
						
						// ConcurrentLinkedQueue<SolutionNode> new_frontier =
						// new ConcurrentLinkedQueue<SolutionNode>();

						node = frontier.remove();
						// System.out.println("element removed: " +
						// node.choice.at);
						if (!node.choice.isDeadend()) {
							curr = node;
							// System.out.println("Elements added: " +
							// expand(node));
							frontier.addAll(expand(node));

							waitLock.notifyAll();

						} else if (maze.display != null) {
							maze.setColor(node.choice.at, 0);
						}

						// frontier = new_frontier;
						if (maze.display != null) {
							maze.display.updateDisplay();
							try {
								Thread.sleep(50);
							} catch (InterruptedException e) {
							}
							// Could use: maze.display.waitForMouse();
							// if we wanted to pause until a mouse button was
							// pressed.
						}
					} catch (SolutionFound e) {

						//System.out.println("Solution from BFS: " + node.choice.at);
						//System.out.println(curr.choice.at);

						if (curr == null) {
							// this only happens if there was a direct path from
							// the start
							// to the end
							found = true;
							System.out.println("Found at 1");
							pathToFullPath(maze.getMoves(maze.getStart()));
							return;
						} else {
							LinkedList<Direction> soln = new LinkedList<Direction>();
							// First save the direction we were going in when
							// the exit was
							// discovered.
							soln.addFirst(exploring);
							while (curr != null) {
								//System.out.println(soln);
								try {
									Choice walkBack = followMark(curr.choice.at, curr.choice.from, 1);
									if (maze.display != null) {
										maze.display.updateDisplay();
									}
									soln.addFirst(walkBack.from);
									curr = curr.parent;
								} catch (SolutionFound e2) {
									// If there is a choice point at the maze
									// entrance, then
									// record
									// which direction we should choose.
									if (maze.getMoves(maze.getStart()).size() > 1)
										soln.addFirst(e2.from);
									if (maze.display != null) {
										markPath(soln, 1);
										maze.display.updateDisplay();
									}
									found = true;
									//System.out.println("Found at 2");
									//System.out.println(soln.toString());
									//pathToFullPath(soln);
									pathToFullPath = pathToFullPath(soln);
									return;
								}
							}
							found = true;
						//	System.out.println("Found at 3");
							//markPath(soln, 1);
							//pathToFullPath = pathToFullPath(soln);
							return;
						}
					} // end of solution found catch block
				} // give up waitLock
			} // end of while loop
		}
	}

	class SolutionNode {
		public SolutionNode parent;

		public Choice choice;

		public SolutionNode(SolutionNode parent, Choice choice) {
			this.parent = parent;
			this.choice = choice;
		}

		public Choice getChoice() {
			return choice;
		}
	}

}
