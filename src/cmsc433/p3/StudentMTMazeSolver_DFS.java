package cmsc433.p3;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * This file needs to hold your solver to be tested. You can alter the class to extend any class that extends
 * MazeSolver. It must have a constructor that takes in a Maze. It must have a solve() method that returns the datatype
 * List<Direction> which will either be a reference to a list of steps to take or will be null if the maze cannot be
 * solved.
 */
public class StudentMTMazeSolver_DFS extends SkippingMazeSolver {
    static LinkedList<Choice> choiceStack = new LinkedList<Choice>();

    static Choice ch;

    static boolean found = false;

    List<Direction> pathToFullPath = new ArrayList();

    Object waitLock = new Object();

    public synchronized void addToChoiceStack(Choice choice) {
        choiceStack.push(choice);
    }

    public synchronized void removeFromStack() {
        choiceStack.pop();
    }

    public synchronized Choice stackPeek() {
        return choiceStack.peek();
    }

    public StudentMTMazeSolver_DFS(Maze maze) {
        super(maze);
    }

    public List<Direction> solve() {

        int threadCount = 10;
        MazeThread[] allThreads = new MazeThread[threadCount];
        try {
            // choiceStack.push(firstChoice(maze.getStart()));
            addToChoiceStack(firstChoice(maze.getStart()));

            for (int i = 0; i < threadCount; i++) {
                allThreads[i] = new MazeThread();

                allThreads[i].start();

            }

            for (int i = 0; i < threadCount; i++) {
                try {
                    allThreads[i].join();
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            return pathToFullPath;
        } catch (SolutionFound e1) {

            System.out.println("66666666666666666666666666666666");

            if (found) {
                System.out.println("77777777777777777777777777777777");
                Iterator<Choice> iter = choiceStack.iterator();
                LinkedList<Direction> solutionPath = new LinkedList<Direction>();
                while (iter.hasNext()) {
                    ch = iter.next();
                    solutionPath.push(ch.choices.peek());
                }

                if (maze.display != null)
                    maze.display.updateDisplay();
                return pathToFullPath(solutionPath);
            }

            // TODO: Implement your code here
            // throw new RuntimeException("Not yet implemented!");
        }
        return null;
    }

    class MazeThread extends Thread {

        public void run() {
            if (found) {
                //System.out.println("yeilding thread");
                this.yield();
                return;
            }
            synchronized (waitLock) {
                // //System.out.println("Thread started" + this);

                while (true) {
                    while (choiceStack.size() == 0) {
                        try {
                            System.out.println("waiting");
                            waitLock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

                    if (found) {
                        // System.out.println("yeilding thread");
                        this.yield();
                        break;
                        // return;
                    }

                    ch = stackPeek();
                    //// System.out.println("ch:" + ch.at);
                    if (ch.isDeadend()) {
                        //// System.out.println("deadend");
                        removeFromStack();
                        if (!choiceStack.isEmpty())
                            stackPeek().choices.pop();
                        // choiceStack.peek().choices.pop();
                        // continue;
                    } else {
                        try {
                            // choiceStack.push(follow(ch.at, ch.choices.peek()));
                            if (found)
                                break;
                            Choice c = follow(ch.at, ch.choices.peek());
                            if (maze.display != null) {
                                maze.setColor(c.at, 2);
                                maze.display.updateDisplay();
                            }

                            addToChoiceStack(c);
                            synchronized (waitLock) {
                                waitLock.notifyAll();
                            }

                        } catch (SolutionFound e) {
                            // System.out.println("Solution Found caught in student thread run");
                            // TODO Auto-generated catch block
                            // e.printStackTrace();
                            found = true;

                            // System.out.println("4444444444444444444444444444");
                            System.out.println("Solution from DFS: " + ch.at);
                            if (found) {
                                // System.out.println("55555555555555555555555555555555555555555555555");
                                Iterator<Choice> iter = choiceStack.iterator();
                                LinkedList<Direction> solutionPath = new LinkedList<Direction>();
                                while (iter.hasNext()) {
                                    ch = iter.next();
                                    solutionPath.push(ch.choices.peek());
                                }

                                if (maze.display != null) {
                                    maze.setColor(ch.at, 2);
                                    maze.display.updateDisplay();
                                }
                                if (maze.display != null) {
                                    markPath(solutionPath, 1);
                                    maze.display.updateDisplay();
                                }
                                found = true;
                                // this.yield();

                                pathToFullPath = pathToFullPath(solutionPath);

                                this.yield();
                            }

                            return;
                        }
                    }
                }
            }
        }
    }
}
